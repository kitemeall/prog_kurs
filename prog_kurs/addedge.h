#ifndef ADDEDGE_H
#define ADDEDGE_H
#include <QDialog>

namespace Ui {
class AddEdge;
}

class AddEdge : public QDialog
{
    Q_OBJECT

public:
    explicit AddEdge(QWidget *parent = 0);
    ~AddEdge();
public slots:
    void lauch();
    void addNode(int number);

private slots:
    void on_comboBox_currentTextChanged(const QString &arg1);

    void on_comboBox_2_currentTextChanged(const QString &arg1);

    void on_buttonBox_accepted();

signals:
    void addEdge(int n1, int n2, int val);
private:
    Ui::AddEdge *ui;
};

#endif // ADDEDGE_H
