#ifndef GRAPHWIDGET_H
#define GRAPHWIDGET_H
#include <iostream>
#include <QGraphicsView>
#include <QWheelEvent>
#include <QMouseEvent>
class Node;
class Edge;
//! [0]
class GraphWidget : public QGraphicsView
{
    Q_OBJECT

public:
    GraphWidget(QWidget *parent = 0);


public slots:
    void zoomIn();
    void zoomOut();

protected:
#ifndef QT_NO_WHEELEVENT
    void wheelEvent(QWheelEvent *event) Q_DECL_OVERRIDE;

#endif
     void mouseDoubleClickEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void drawBackground(QPainter *painter, const QRectF &rect) Q_DECL_OVERRIDE;
    void scaleView(qreal scaleFactor);

private:
    int timerId;
    Node *centerNode;
    void addNode (QPointF pos);
    int itemNumber;
    QList <Node *> nodeList;
    QList < Edge*> edgeList;
    std::vector <int> currentPath;
    void drawPath();

public slots:
    void addEdge (int n1, int n2, int val);
    void startAlgo(int n1, int n2);

signals:
    void addEdgeLauch();
    void nodeAdded(int number);
    void setPathLength(QString s = "inf");
};
//! [0]

#endif // GRAPHWIDGET_H
