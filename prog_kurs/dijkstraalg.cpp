#include "dijkstraalg.h"
#include <vector>
#include <algorithm>
using namespace std;
DijkstraAlg::DijkstraAlg()
{

}
vector <int> DijkstraAlg::findPath(vector < vector < pair<int,int> > > g ,int s, int f)
{
    int n = g.size();
    vector<int> d (n, INF),  p (n, -1);
        d[s] = 0;
        vector<char> u (n);
        for (int i=0; i<n; ++i) {
            int v = -1;
            for (int j=0; j<n; ++j)
                if (!u[j] && (v == -1 || d[j] < d[v]))
                    v = j;
            if (d[v] == INF)
            {
               break;
            }

            u[v] = true;

            for (size_t j=0; j<g[v].size(); ++j) {
                int to = g[v][j].first,
                    len = g[v][j].second;
                if (d[v] + len < d[to]) {
                    d[to] = d[v] + len;
                    p[to] = v;
                }
            }
        }

        vector<int> path;
        if(p[f] == -1)
            return path;

        for (int v=f; v!=s; v=p[v])
            path.push_back (v);
        path.push_back (s);
        reverse (path.begin(), path.end());
        return path;
}

