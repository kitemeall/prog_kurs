#ifndef ALGORITHSTARTWINDOW_H
#define ALGORITHSTARTWINDOW_H

#include <QWidget>

namespace Ui {
class AlgorithStartWindow;
}

class AlgorithStartWindow : public QWidget
{
    Q_OBJECT

public:
    explicit AlgorithStartWindow(QWidget *parent = 0);
    ~AlgorithStartWindow();

private:
    Ui::AlgorithStartWindow *ui;
signals:
    void startAlgo(int n1, int n2);

public slots:
    void addNode(int number);
    void setLen(QString l);
private slots:
    void on_comboBox_currentTextChanged(const QString &arg1);
    void on_comboBox_2_currentTextChanged(const QString &arg1);
    void on_pushButton_clicked();
};

#endif // ALGORITHSTARTWINDOW_H
