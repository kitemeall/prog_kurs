#include "addedge.h"
#include "ui_addedge.h"

AddEdge::AddEdge(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddEdge)
{
    ui->setupUi(this);
}

AddEdge::~AddEdge()
{
    delete ui;
}

void AddEdge::lauch()
{
    this->show();
}
void AddEdge::addNode(int number)
{
    QString val = QString::number(number);
    ui->comboBox->addItem(val);
    ui->comboBox_2->addItem(val);
}


void AddEdge::on_comboBox_currentTextChanged(const QString &arg1)
{
    if (arg1 == ui->comboBox_2->itemText(ui->comboBox_2->currentIndex()))
    {
        ui->spinBox->setEnabled(false);
        ui->buttonBox->setEnabled(false);
    }

    else
    {
        ui->spinBox->setEnabled(true);
        ui->buttonBox->setEnabled(true);

    }
}

void AddEdge::on_comboBox_2_currentTextChanged(const QString &arg1)
{
    if (arg1 == ui->comboBox->itemText(ui->comboBox->currentIndex()))
    {
        ui->spinBox->setEnabled(false);
        ui->buttonBox->setEnabled(false);
    }
    else
    {
        ui->spinBox->setEnabled(true);
        ui->buttonBox->setEnabled(true);

    }}

void AddEdge::on_buttonBox_accepted()
{
    QString s = ui->comboBox->itemText(ui->comboBox->currentIndex());
    bool ok1, ok2;
    int n1 = s.toInt(&ok1);
    s = ui->comboBox_2->itemText(ui->comboBox_2->currentIndex());
    int n2 = s.toInt(&ok2);
    int val = ui->spinBox->value();
    if(ok1 && ok2)
        emit addEdge(n1, n2, val);

}
