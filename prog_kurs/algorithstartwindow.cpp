#include "algorithstartwindow.h"
#include "ui_algorithstartwindow.h"

AlgorithStartWindow::AlgorithStartWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AlgorithStartWindow)
{
    ui->setupUi(this);
}

AlgorithStartWindow::~AlgorithStartWindow()
{
    delete ui;
}
void AlgorithStartWindow::addNode(int number)
{
    QString val = QString::number(number);
    ui->comboBox->addItem(val);
    ui->comboBox_2->addItem(val);
}

void AlgorithStartWindow::on_comboBox_currentTextChanged(const QString &arg1)
{
    if (arg1 == ui->comboBox_2->itemText(ui->comboBox_2->currentIndex()))
    {
        ui->pushButton->setEnabled(false);
    }

    else
    {
        ui->pushButton->setEnabled(true);

    }
}

void AlgorithStartWindow::on_comboBox_2_currentTextChanged(const QString &arg1)
{
    if (arg1 == ui->comboBox->itemText(ui->comboBox->currentIndex()))
    {
        ui->pushButton->setEnabled(false);
    }
    else
    {
        ui->pushButton->setEnabled(true);

    }

}

void AlgorithStartWindow::on_pushButton_clicked()
{
    QString s = ui->comboBox->itemText(ui->comboBox->currentIndex());
    bool ok1, ok2;
    int n1 = s.toInt(&ok1);
    s = ui->comboBox_2->itemText(ui->comboBox_2->currentIndex());
    int n2 = s.toInt(&ok2);
    if(ok1 && ok2)
        emit startAlgo(n1, n2);
}

void AlgorithStartWindow::setLen(QString l)
{
    ui->lineEdit->setText(l);
}

