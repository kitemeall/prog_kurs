#include "graphwidget.h"
#include "edge.h"
#include "node.h"
#include "addedge.h"
#include "algorithstartwindow.h"
#include "dijkstraalg.h"
#include <math.h>
#include <QDebug>
#include <QEvent>
#include <QApplication>
#include <QMessageBox>

//! [0]
GraphWidget::GraphWidget(QWidget *parent)
    : QGraphicsView(parent), timerId(0), itemNumber(1)
{
    QGraphicsScene *scene = new QGraphicsScene(this);
    scene->setItemIndexMethod(QGraphicsScene::NoIndex);
    scene->setSceneRect(0, 0,600, 600 );
    setScene(scene);

    setCacheMode(CacheBackground);
    setViewportUpdateMode(BoundingRectViewportUpdate);
    setRenderHint(QPainter::Antialiasing);
    setTransformationAnchor(AnchorUnderMouse);
    scale(qreal(0.9), qreal(0.9));
    setMinimumSize(600, 600);

    AddEdge* edgeAdder = new AddEdge(this);
    connect(this, SIGNAL(addEdgeLauch()), edgeAdder, SLOT(lauch()));
    connect(this, SIGNAL(nodeAdded(int)), edgeAdder, SLOT(addNode(int)));
    connect(edgeAdder, SIGNAL(addEdge(int,int,int)), this ,SLOT(addEdge(int,int,int)));

    AlgorithStartWindow* algWindow = new AlgorithStartWindow(this);
    algWindow->show();
    connect(this, SIGNAL(nodeAdded(int)), algWindow, SLOT(addNode(int)));
    connect(algWindow, SIGNAL(startAlgo(int,int)),this, SLOT(startAlgo(int, int)));
    connect(this, SIGNAL(setPathLength(QString)), algWindow, SLOT(setLen(QString)));

}



void GraphWidget::wheelEvent(QWheelEvent *event)
{
    scaleView(pow((double)2, -event->delta() / 240.0));
}

void GraphWidget::mouseDoubleClickEvent(QMouseEvent *event)
{


    QPointF point = mapToScene(event->pos());

    QGraphicsItem* it =  scene()->itemAt(point, QGraphicsView::transform());

    if(!it)
    {
        addNode(point);
    }
    else if (qgraphicsitem_cast <Node*> (it))
        emit addEdgeLauch();

}

void GraphWidget::addNode (QPointF pos)
{
    Node* n = new Node(this, itemNumber);
    scene()->addItem(n);
    n->setPos(pos);
    emit nodeAdded(itemNumber);
    itemNumber ++;
    nodeList.push_back(n);

}

void GraphWidget::addEdge (int n1, int n2, int val)
{
    Node *nn1 = 0;
    Node *nn2 = 0;

    for (QList <Node *>::iterator i = nodeList.begin();
         i != nodeList.end(); i++)
    {
        int n = (*i)->getNumber();
        if( n == n1)
            nn1 = (*i);
        else if(n == n2)
            nn2 = (*i);
    }

    if(nn1 && nn2)
    {
        Edge * e = new Edge(nn1, nn2, val);
        edgeList.push_back(e);
        scene()->addItem(e);
    }
}




void GraphWidget::drawBackground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(rect);

    // Shadow
    QRectF sceneRect = this->sceneRect();


    // Fill
    painter->fillRect(rect.intersected(sceneRect),QBrush(Qt::white));
        painter->setBrush(Qt::NoBrush);

        QRectF textRect(sceneRect.left() + 150, sceneRect.top()-25 ,
                        sceneRect.width() - 146, sceneRect.height() - 4);
        QString message(tr("Double click on white space to add node.\n"
                           "Double click on node to add edge."));

        QFont font = painter->font();
        font.setBold(true);
        font.setPointSize(14);

        painter->setFont(font);
        painter->setPen(Qt::black);
        painter->drawText(textRect, message);
}
//! [6]

//! [7]
void GraphWidget::scaleView(qreal scaleFactor)
{
    qreal factor = transform().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
    if (factor < 0.5 || factor > 5)
        return;

    scale(scaleFactor, scaleFactor);
}
//! [7]

void GraphWidget::startAlgo(int n1, int n2)
{
    qDebug()<<"algo started" << n1 << n2;
    vector < vector < pair<int,int> > > g;
    for (QList <Node *>::iterator i = nodeList.begin();
         i != nodeList.end(); i++)
    {
        vector < pair<int,int> > p = (*i)->getEdges();
        for (int j = 0; j < p.size(); j++)
            p[j].first --;
        g.push_back(p);

    }

    currentPath = DijkstraAlg::findPath(g, n1-1, n2-1);

    if(currentPath.empty())
    {
        QMessageBox::information(this,"Path not fount", "Unable to get to this node! ");
        emit setPathLength("inf");
    }
    else
    {
        for (int i = 0; i < currentPath.size(); i++)
            currentPath[i] ++;
        drawPath();
    }
}




void GraphWidget::zoomIn()
{
    scaleView(qreal(1.2));
}

void GraphWidget::zoomOut()
{
    scaleView(1 / qreal(1.2));
}

void GraphWidget::drawPath()
{
    int len = 0;
    for(QList <Edge*>::iterator i = edgeList.begin();
        i != edgeList.end(); i++)
    {
        Edge* edge = (*i);
        (edge)->setMarked(false);
        for(int j = 1; j < currentPath.size(); j++)
        {
            if(edge->sourceNode()->getNumber() == currentPath[j-1] &&
                    edge->destNode()->getNumber() == currentPath[j])
            {
                edge->setMarked(true);
                len += edge->getDistance();
        }
        }
    }
    scene()->update();
    emit setPathLength(QString::number(len));
}
