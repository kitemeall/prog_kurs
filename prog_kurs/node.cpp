#include "edge.h"
#include "node.h"
#include "graphwidget.h"

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QStyleOption>


Node::Node(GraphWidget *graphWidget, int num)
    : graph(graphWidget)
{
    setFlag(ItemIsMovable);
    setFlag(ItemSendsGeometryChanges);
    setCacheMode(DeviceCoordinateCache);
    setZValue(-1);
    number = num;
}

void Node::addEdge(Edge *edge)
{
    edgeList << edge;
    edge->adjust();
}

int Node::getNumber()
{
    return number;
}

QList<Edge *> Node::edges() const
{
    return edgeList;
}

QRectF Node::boundingRect() const
{
    qreal adjust = 2;
    return QRectF( -10 - adjust, -10 - adjust, 23 + adjust, 23 + adjust);
}

QPainterPath Node::shape() const
{
    QPainterPath path;
    path.addEllipse(-10, -10, 20, 20);
    return path;
}

void Node::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *)
{
    painter->setPen(Qt::NoPen);
    painter->setBrush(Qt::blue);

    painter->setPen(QPen(Qt::black, 0));
    painter->drawEllipse(-10, -10, 20, 20);
    painter->setPen(QPen(Qt::white, 0));
    painter->drawText(-10, -10, 20,20,Qt::AlignCenter,QString::number(number));
}

QVariant Node::itemChange(GraphicsItemChange change, const QVariant &value)
{
    switch (change) {
    case ItemPositionHasChanged:
        foreach (Edge *edge, edgeList)
            edge->adjust();
        break;
    default:
        break;
    };

    return QGraphicsItem::itemChange(change, value);
}

void Node::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    update();
    QGraphicsItem::mousePressEvent(event);
}

void Node::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    update();
    QGraphicsItem::mouseReleaseEvent(event);
}

 std::vector <std::pair <int, int > > Node::getEdges()
 {
      std::vector <std::pair <int, int > >  v;

     for (QList  <Edge *>::iterator i = edgeList.begin();
          i != edgeList.end(); i++)
     {
         if((*i)->sourceNode() == this)
         {
             int to = (*i)->destNode()->getNumber();
             int cost = (*i)->getDistance();
             v.push_back(std::make_pair(to, cost));
         }
     }
     return v;
 }

