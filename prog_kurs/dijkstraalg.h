#ifndef DIJKSTRAALG_H
#define DIJKSTRAALG_H
#include <vector>
using namespace std;
class DijkstraAlg
{
public :
    DijkstraAlg();
    static vector <int> findPath(vector < vector < pair<int,int> > > g, int n1, int n2);

 private:
    static const int INF = 1000000000;


};

#endif // DIJKSTRAALG_H
