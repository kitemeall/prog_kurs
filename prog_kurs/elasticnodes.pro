QT += widgets

HEADERS += \
        edge.h \
        node.h \
        graphwidget.h \
    addedge.h \
    algorithstartwindow.h \
    dijkstraalg.h

SOURCES += \
        edge.cpp \
        main.cpp \
        node.cpp \
        graphwidget.cpp \
    addedge.cpp \
    algorithstartwindow.cpp \
    dijkstraalg.cpp

# install
target.path = $$[QT_INSTALL_EXAMPLES]/widgets/graphicsview/elasticnodes
INSTALLS += target

FORMS += \
    addedge.ui \
    algorithstartwindow.ui
